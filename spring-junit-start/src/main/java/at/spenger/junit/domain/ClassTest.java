package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import at.spenger.junit.domain.Person.Sex;

public class ClassTest {

	Club c = new Club();
	
	@Test
	 public void testEnter() {
	  final LocalDate ld = LocalDate.of(1979, 6, 17);
	  final LocalDate ld2 = LocalDate.of(1987, 9, 4);
	  final LocalDate ld3 = LocalDate.of(1982, 2, 26);
	  final LocalDate ld4 = LocalDate.of(1991, 11, 30);
	  Sex sf = Sex.FEMALE;
	  Sex sm = Sex.MALE;
	  
	  c.enter(new Person("Laura", "Sevcik", ld , sm));
	  c.enter(new Person("Sophie", "Cisda", ld2 , sf));
	  c.enter(new Person("Isaac", "Newton", ld3 , sm));
	  c.enter(new Person("Julia", "Berger", ld4 , sf));
	 }

	 @Test
	 public void testNumberOf() {
	  Club c = new Club();
	  
	  int erg = c.numberOf();
	  int exp = 0;
	  assertEquals(exp, erg);
	 }
	 
	 @Test
	 public void testAverageAge(){
	  
	  testEnter();
	  c.durchschnittsalter();
	  
	 }
	 
	 @Test
	 public void testSort(){
	  testEnter();
	  c.sortPeople(); 
	 }

	 @Test
	 public void testDelete(){
	  Person p = new Person("Julia", "Zin�cker", LocalDate.of(1995, 8, 29), Sex.FEMALE);
	  c.enter(p);
	  testEnter();
	  c.exit(p);
	 }
	 
	 @Test
	 public void testRelation(){
	  testEnter();
	  c.relationMaleToFemale();
	 }
	 
	 //--------
	 
	 @Test
	 public void testSortCollect(){
	  testEnter();
	  c.sortName();
	  List<Person> l = c.getPersons();
	  System.out.println(l.toString());
	  
	  assertTrue(l.get(0).getLastName().equals("Berger"));
	  assertTrue(l.get(1).getLastName().equals("Csida"));
	  assertTrue(l.get(2).getLastName().equals("Newton"));
	  assertTrue(l.get(3).getLastName().equals("Sevcik"));
	  
	 }
	 
	 
	 //---- JAVA8 TESTS -----------
	 
	 @Test
	 public void testAverageAgeJava8(){
	  testEnter();
	  c.durchschnittsalterJava8();
	 }
	 
	 /*
	 @Test
	 public void testSortJava8(){
	  testEnter();
	  c.sortNameAge();
	  
	  List<Person> l = a.getPersons();
	  assertTrue(l.get(0).getLastName().equals("Berger"));
	  assertTrue(l.get(1).getLastName().equals("Csida"));
	  assertTrue(l.get(2).getLastName().equals("Newton"));
	  assertTrue(l.get(3).getLastName().equals("Sevcik"));

	 }
	 */
	 
	 @Test
	 public void testGroupByJava8(){
	  testEnter();
	  Map <Object, Long> m = c.groupbyBirthYear();
	  
	  List<Person> l = c.getPersons();
	  for(Person p : l){
	   assertTrue(m.get(p.getBirthYear()).equals(1));
	  }
	 }
	 
	 @Test
	 public void testDeleteJava8(){
	  Person p = new Person("Julia", "Zin�cker", LocalDate.of(1995, 8, 29), Sex.FEMALE);
	  c.enter(p);
	  testEnter();
	  c.exitJava8(p);
	  
	  
	  List<Person> l = c.getPersons();
	  assertTrue(l.get(3).getLastName().equals("Sevcik"));
	  assertTrue(l.get(1).getLastName().equals("Cisda"));
	  assertTrue(l.get(2).getLastName().equals("Newton"));
	  assertTrue(l.get(0).getLastName().equals("Berger"));
	 }
	 
	 public void testBornInJava8(){
	  testEnter();
	  c.bornInJava8();
	 }
}
