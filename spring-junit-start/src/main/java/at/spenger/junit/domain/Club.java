package at.spenger.junit.domain;

import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import at.spenger.junit.domain.Person.Sex;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}
	
	public int durchschnittsalter(){
		  int totalage = 0;
		  int avg = 0;
		  int no = 0;
		  
		  for(Person p : l){
		   Calendar birthDay = Calendar.getInstance();
		   birthDay.set(Calendar.YEAR, p.getBirthday().getYear());
		   birthDay.set(Calendar.MONTH, p.getBirthday().getMonthValue());
		   birthDay.set(Calendar.DATE, p.getBirthday().getDayOfMonth());
		    
		   long currentTime = System.currentTimeMillis();
		   Calendar currentDay = Calendar.getInstance();
		   currentDay.setTimeInMillis(currentTime);
		    
		   //Get difference between years
		   int years = currentDay.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
		   
		   totalage +=years;
		   no++;
		  }
		  
		  avg = totalage/no;
		  System.out.println(avg);
		  return avg;
	}
	
	public List<Person> exit(Person p){
		l.remove(p);
		return l;
	}
	
	public List<Person> sortPeople(){
		Collections.sort(l, new Comparator<Person>() {

	         public int compare(Person o1, Person o2) {
	             return o2.getLastName().compareTo(o1.getLastName());
	         }
	     });
	  
	  System.out.println(l.toString());
	  
	  return l;
	}
	
	public int relationMaleToFemale(){
		  int male = 0;
		  int female = 0;
		  
		  for(Person p:l){
		   if(p.getSex() == Sex.FEMALE){
		    female++;
		   }else{
		    male++;
		   }
		  }
		  
		  int rel = female/male;
		  System.out.println("Relation male to female -  1:" + rel);
		  return rel;
	}
	
	  //collector macht stream zu einer liste zur�ck
	  public void sortName(){
	   l = l.stream().sorted((x, y) -> x.getLastName().compareTo(y.getLastName())).collect(Collectors.toList());
	  }
	  
	  
	 // JAVA8 TESTS
	  
	  public double durchschnittsalterJava8(){
	   double average = l.stream()
	     .filter(p -> p.getSex() == Person.Sex.MALE)
	     .mapToInt(Person::getAge) // or lambda expression: e -> e.getAge()
	     .average() .getAsDouble();
	   System.out.println(average);
	   return average;
	  }
	  
	  
	  //byAge: nimmt int nicht
	  //Sortieren nach Name und Alter
	  /*
	  public List<Person> sortNameAge(){
	   Comparator<Person> byName = (x, y) -> x.getLastName().compareTo(y.getLastName());

	      Comparator<Person> byAge = (x, y) -> x.getAge().compareTo(y.getAge());

	      
	      l.stream().sorted(byName.thenComparing(byAge))
	              .forEach(e -> System.out.println(e));
	   
	   return l;
	  }
	  */
	  
	  public Map<Object, Long> groupbyBirthYear(){
		  Map<Object, Long> counted = l.stream().collect(Collectors.groupingBy(p -> p.getBirthYear(), Collectors.counting()));
		  System.out.println(counted);
		  return counted;
	  }
	  
	  public List<Person> exitJava8(Person p){
	   l.removeIf(x -> x.getLastName().equals(p.getLastName()));
	   return l;
	  }
	  
	  //1.Extra Methode
	  public void bornInJava8(){
	   l.stream().forEach((person) -> System.out.println(person.getLastName() + " was born in " + person.getBirthYear()));
	  }
}
